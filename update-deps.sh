#!/bin/sh

VERVIS='https://vervis.peers.community/repos'

DEPS="vlBxr   dvara                    \n
      nrAMr   hit-graph                \n
      RvP1E   hit-harder               \n
      rL9jo   hit-network              \n
      9EmyE   darcs-lights             \n
      v3e8r   http-client-signature    \n
      vM99v   http-signature           \n
      6r4Ao   ssh                      \n
      oq5mo   persistent-graph         \n
      0rd3E   persistent-migration     \n
      4oRNo   persistent-email-address \n
      AoO7o   time-interval-aeson      \n
      r6WGo   yesod-http-signature     \n
      2vanE   yesod-mail-send"

mkdir -p lib
cd lib
echo $DEPS | while read -r dep
do
    slug=$(echo $dep | cut --fields=1 --delimiter=' ')
    name=$(echo $dep | cut --fields=2 --delimiter=' ')

    echo
    if [ -d "$name" ]; then
        echo "Updating dependency $name"
        darcs pull --repodir="$name" --all
    else
        echo "Cloning dependency '$name' from $VERVIS/$slug"
        darcs clone "$VERVIS/$slug" "$name"
    fi
done
