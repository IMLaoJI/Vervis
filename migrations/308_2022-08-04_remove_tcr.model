Ticket
FollowerSet
RemoteActor
RemoteObject
Person
OutboxItem
RemoteActivity
Project
Repo

LocalTicket
    ticket    TicketId
    discuss   DiscussionId
    followers FollowerSetId

    UniqueLocalTicket           ticket
    UniqueLocalTicketDiscussion discuss
    UniqueLocalTicketFollowers  followers

TicketProjectRemote
    ticket  TicketAuthorLocalId
    tracker RemoteActorId
    project RemoteObjectId Maybe -- specify if not same as tracker
                                 -- For MRs it may be either a remote repo or
                                 -- a branch of it

    UniqueTicketProjectRemote ticket

TicketAuthorLocal
    ticket LocalTicketId
    author PersonId
    open   OutboxItemId

    UniqueTicketAuthorLocal     ticket
    UniqueTicketAuthorLocalOpen open

Message
    created UTCTime
    source  Text               -- Pandoc Markdown
    content Text               -- HTML
    parent  MessageId    Maybe
    root    DiscussionId

LocalMessage
    author         PersonId
    rest           MessageId
    create         OutboxItemId
    unlinkedParent FedURI    Maybe

    UniqueLocalMessage rest
    UniqueLocalMessageCreate create

RemoteMessage
    author     RemoteActorId
    ident      RemoteObjectId
    rest       MessageId
    create     RemoteActivityId
    lostParent FedURI            Maybe

    UniqueRemoteMessageIdent  ident
    UniqueRemoteMessage       rest
    UniqueRemoteMessageCreate create

Follow
    person PersonId
    target FollowerSetId
    public Bool
    follow OutboxItemId
    accept OutboxItemId

    UniqueFollow person target
    UniqueFollowFollow follow
    UniqueFollowAccept accept

RemoteFollow
    actor  RemoteActorId
    target FollowerSetId
    public Bool
    follow RemoteActivityId
    accept OutboxItemId

    UniqueRemoteFollow actor target
    UniqueRemoteFollowFollow follow
    UniqueRemoteFollowAccept accept

RemoteTicket
    ticket  TicketAuthorRemoteId
    ident   RemoteObjectId
    discuss RemoteDiscussionId

    UniqueRemoteTicket        ticket
    UniqueRemoteTicketIdent   ident
    UniqueRemoteTicketDiscuss discuss

Discussion

RemoteDiscussion
    ident   RemoteObjectId
    discuss DiscussionId

    UniqueRemoteDiscussionIdent ident
    UniqueRemoteDiscussion      discuss

TicketAuthorRemote
    ticket TicketContextLocalId
    author RemoteActorId
    open   RemoteActivityId

    UniqueTicketAuthorRemote     ticket
    UniqueTicketAuthorRemoteOpen open

TicketContextLocal
    ticket  TicketId
    accept  OutboxItemId

    UniqueTicketContextLocal       ticket
    UniqueTicketContextLocalAccept accept

TicketProjectLocal
    context TicketContextLocalId
    project ProjectId

    UniqueTicketProjectLocal context

TicketRepoLocal
    context TicketContextLocalId
    repo    RepoId
    branch  Text Maybe

    UniqueTicketRepoLocal context
