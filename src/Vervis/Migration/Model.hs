{- This file is part of Vervis.
 -
 - Written in 2018, 2019, 2020, 2022 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Migration.Model
    {-
    ( EntityField (..)
    , Unique (..)
    , Sharer2016
    , Project2016
    , Workflow2016Generic (..)
    , Workflow2016
    , VerifKey2019Generic (..)
    , VerifKey2019
    , VerifKeySharedUsage2019Generic (..)
    , VerifKeySharedUsage2019
    , Message2019Generic (..)
    , Message2019
    , LocalMessage2019Generic (..)
    , LocalMessage2019
    , FollowerSet2019Generic (..)
    , Ticket2019
    , Sharer201905Generic (..)
    , Person201905Generic (..)
    , OutboxItem201905Generic (..)
    , OutboxItem201905
    , LocalMessage201905Generic (..)
    , LocalMessage201905
    , Message201905Generic (..)
    , Project201905Generic (..)
    , Ticket201905Generic (..)
    , Instance201905Generic (..)
    , RemoteDiscussion201905Generic (..)
    , RemoteMessage201905Generic (..)
    , Message201906Generic (..)
    , Message201906
    , Ticket201906Generic (..)
    , Ticket201906
    , Ticket20190606Generic (..)
    , Ticket20190606
    , TicketAuthorLocal20190606Generic (..)
    , Person20190607Generic (..)
    , Person20190607
    , Inbox20190607Generic (..)
    , InboxItemLocal20190607Generic (..)
    , InboxItemLocal20190607
    , InboxItemRemote20190607Generic (..)
    , InboxItemRemote20190607
    , Project20190609
    , Inbox20190609Generic (..)
    , InboxItem2019FillGeneric (..)
    , InboxItem2019Fill
    , InboxItemLocal2019FillGeneric (..)
    , InboxItemRemote2019FillGeneric (..)
    , Project2019FillGeneric (..)
    , Ticket2019FillGeneric (..)
    , Message2019FillGeneric (..)
    , LocalMessage2019FillGeneric (..)
    , RemoteMessage2019FillGeneric (..)
    , FollowerSet20190610Generic (..)
    , Project20190610
    , Sharer20190612Generic (..)
    , Person20190612Generic (..)
    , OutboxItem20190612Generic (..)
    , Inbox20190612Generic (..)
    , InboxItem20190612Generic (..)
    , InboxItemLocal20190612Generic (..)
    , Project20190612Generic (..)
    , Ticket20190612Generic (..)
    , Ticket20190612
    , TicketAuthorLocal20190612Generic (..)
    , Person20190615Generic (..)
    , Person20190615
    , Outbox20190615Generic (..)
    , OutboxItem20190615Generic (..)
    , OutboxItem20190615
    , Project20190616Generic (..)
    , Project20190616
    , Outbox20190616Generic (..)
    , Sharer20190624Generic (..)
    , Person20190624Generic (..)
    , Outbox20190624Generic (..)
    , OutboxItem20190624Generic (..)
    , Inbox20190624Generic (..)
    , InboxItem20190624Generic (..)
    , InboxItemLocal20190624Generic (..)
    , Project20190624Generic (..)
    , Ticket20190624Generic (..)
    , Ticket20190624
    , TicketAuthorLocal20190624Generic (..)
    , Sharer127Generic (..)
    , Person127Generic (..)
    , Outbox127Generic (..)
    , Inbox127Generic (..)
    , Project127Generic (..)
    , Ticket127Generic (..)
    , TicketDependency127Generic (..)
    , TicketDependency127
    , Inbox130Generic (..)
    , FollowerSet130Generic (..)
    , Repo130
    , Person130
    , Outbox138Generic (..)
    , Repo138
    , Instance152Generic (..)
    , RemoteObject152Generic (..)
    , RemoteActivity152Generic (..)
    , RemoteActivity152
    , Instance159Generic (..)
    , RemoteObject159Generic (..)
    , RemoteActor159Generic (..)
    , RemoteActor159
    , UnfetchedRemoteActor159Generic (..)
    , UnfetchedRemoteActor159
    , RemoteCollection159Generic (..)
    , RemoteCollection159
    , Ticket189
    , Ticket189Generic (..)
    , LocalTicket189Generic (..)
    , Sharer194Generic (..)
    , Outbox194Generic (..)
    , OutboxItem194Generic (..)
    , Inbox194Generic (..)
    , FollowerSet194Generic (..)
    , Project194Generic (..)
    , Workflow194Generic (..)
    , Ticket194Generic (..)
    , LocalTicket194Generic (..)
    , TicketAuthorLocal194
    , TicketAuthorLocal194Generic (..)
    , Discussion194Generic (..)
    , Ticket201
    , Ticket201Generic (..)
    , TicketProjectLocal201Generic (..)
    , Sharer205Generic (..)
    , Outbox205Generic (..)
    , OutboxItem205Generic (..)
    , Inbox205Generic (..)
    , FollowerSet205Generic (..)
    , Project205Generic (..)
    , Workflow205Generic (..)
    , Ticket205Generic (..)
    , TicketProjectLocal205Generic (..)
    , TicketAuthorRemote205
    , TicketAuthorRemote205Generic (..)
    , Instance215Generic (..)
    , RemoteObject215Generic (..)
    , RemoteDiscussion215
    , RemoteDiscussion215Generic (..)
    , TicketUnderProject223Generic (..)
    , Instance227Generic (..)
    , RemoteObject227Generic (..)
    , RemoteMessage227
    , RemoteMessage227Generic (..)
    , RemoteTicket238
    , RemoteTicket238Generic (..)
    , Instance238Generic (..)
    , RemoteObject238Generic (..)
    , Discussion238Generic (..)
    , RemoteDiscussion238Generic (..)
    , Forwarding241
    , Forwarding241Generic (..)
    , ForwarderProject241Generic (..)
    , TicketContextLocal247
    , TicketContextLocal247Generic (..)
    , TicketProjectLocal247Generic (..)
    , OutboxItem255Generic (..)
    , Person255Generic (..)
    , TicketDependency255
    , TicketDependency255Generic (..)
    , TicketDependencyAuthorLocal255Generic (..)
    , RemoteTicket260Generic (..)
    , LocalTicketDependency260
    , LocalTicketDependency260Generic (..)
    , TicketDependencyChildLocal260Generic (..)
    , TicketDependencyChildRemote260Generic (..)
    , Discussion263Generic (..)
    , FollowerSet263Generic (..)
    , Ticket263Generic (..)
    , LocalTicket263Generic (..)
    , LocalTicketDependency263
    , LocalTicketDependency263Generic (..)
    , Outbox266Generic (..)
    , OutboxItem266Generic (..)
    , LocalTicketDependency266
    , LocalTicketDependency266Generic (..)
    , LocalTicket266Generic (..)
    , TicketContextLocal266Generic (..)
    , TicketUnderProject266Generic (..)
    , TicketProjectLocal266Generic (..)
    , Project266Generic (..)
    , TicketResolve276Generic (..)
    , TicketResolveLocal276Generic (..)
    , Ticket276Generic (..)
    , LocalTicket276
    , LocalTicket276Generic (..)
    , Person276Generic (..)
    , OutboxItem276Generic (..)
    , TicketProjectLocal276Generic (..)
    , Project276Generic (..)
    , Ticket280Generic (..)
    , Bundle280Generic (..)
    , Patch280
    , Patch280Generic (..)
    , Repo282
    , Repo282Generic (..)
    , Collab285Generic (..)
    , CollabRecipLocal285Generic (..)
    , CollabRoleLocal285Generic (..)
    , CollabSenderLocal285Generic (..)
    , CollabTopicLocalProject285Generic (..)
    , CollabTopicLocalRepo285Generic (..)
    , OutboxItem285Generic (..)
    , Project285Generic (..)
    , ProjectCollab285
    , ProjectCollab285Generic (..)
    , Repo285Generic (..)
    , RepoCollab285
    , RepoCollab285Generic (..)
    , Project289
    , Inbox289Generic (..)
    , Outbox289Generic (..)
    , FollowerSet289Generic (..)
    , Actor289Generic (..)
    , Project289Generic (..)
    , Outbox297Generic (..)
    , OutboxItem297Generic (..)
    , Project297
    , Project297Generic (..)
    , Person297Generic (..)
    , CollabTopicLocalProject300
    , CollabTopicLocalProject300Generic (..)
    , CollabTopicLocalRepo300
    , CollabTopicLocalRepo300Generic (..)
    , CollabRecipLocal300Generic (..)
    , Person300Generic (..)
    , Project300Generic (..)
    , Repo300Generic (..)
    , CollabFulfillsLocalTopicCreation300Generic (..)
    )
    -}
where

import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Time (UTCTime)
import Database.Persist.Class (EntityField, Unique)
import Database.Persist.EmailAddress ()
import Database.Persist.Schema.Types (Entity)
import Database.Persist.Schema.SQL ()
import Database.Persist.Schema.TH (makeEntitiesMigration)
import Database.Persist.Sql (SqlBackend)
import Text.Email.Validate (EmailAddress)
import Web.Text (HTML, PandocMarkdown)

import Crypto.ActorKey
import Development.PatchMediaType
import Development.PatchMediaType.Persist

import Vervis.FedURI
import Vervis.Migration.TH (schema)
import Vervis.Model.Group
import Vervis.Model.Ident
import Vervis.Model.Role
import Vervis.Model.TH
import Vervis.Model.Ticket
import Vervis.Model.Workflow

-- For migrations 77, 114

import Data.Int

import Database.Persist.JSON
import Network.FedURI
import Web.ActivityPub

type PersistActivity = PersistJSON (Doc Activity URIMode)

makeEntitiesMigration "2016"
    $(modelFile "migrations/2016_09_01_just_workflow_prepare.model")

makeEntitiesMigration "2018"
    $(modelFile "migrations/2019_01_28_project_collabs.model")

makeEntitiesMigration "2019"
    $(modelFile "migrations/2019_02_03_verifkey.model")

makeEntitiesMigration "2019"
    $(modelFile "migrations/2019_03_18_message.model")

makeEntitiesMigration "2019"
    $(modelFile "migrations/2019_03_30_follower_set.model")

makeEntitiesMigration "201905"
    $(modelFile "migrations/2019_05_24.model")

makeEntitiesMigration "201906"
    $(modelFile "migrations/2019_06_02.model")

makeEntitiesMigration "201906"
    $(modelFile "migrations/2019_06_03.model")

makeEntitiesMigration "20190606"
    $(modelFile "migrations/2019_06_06_mig.model")

makeEntitiesMigration "20190607"
    $(modelFile "migrations/2019_06_07.model")

makeEntitiesMigration "20190609"
    $(modelFile "migrations/2019_06_09.model")

makeEntitiesMigration "2019Fill"
    $(modelFile "migrations/2019_06_09_fill.model")

makeEntitiesMigration "20190610"
    $(modelFile "migrations/2019_06_10.model")

makeEntitiesMigration "20190612"
    $(modelFile "migrations/2019_06_12.model")

makeEntitiesMigration "20190615"
    $(modelFile "migrations/2019_06_15.model")

makeEntitiesMigration "20190616"
    $(modelFile "migrations/2019_06_16.model")

makeEntitiesMigration "20190624"
    $(modelFile "migrations/2019_06_24.model")

makeEntitiesMigration "127"
    $(modelFile "migrations/2019_07_11.model")

makeEntitiesMigration "130"
    $(modelFile "migrations/2019_09_06.model")

makeEntitiesMigration "138"
    $(modelFile "migrations/2019_09_10.model")

makeEntitiesMigration "152"
    $(modelFile "migrations/2019_11_04_remote_activity_ident.model")

makeEntitiesMigration "159"
    $(modelFile "migrations/2019_11_05_remote_actor_ident.model")

makeEntitiesMigration "189" $(modelFile "migrations/2020_02_05_mig.model")

makeEntitiesMigration "194"
    $(modelFile "migrations/2020_02_06_tal_point_to_lt.model")

makeEntitiesMigration "201"
    $(modelFile "migrations/2020_02_07_tpl_mig.model")

makeEntitiesMigration "205"
    $(modelFile "migrations/2020_02_08_tar_point_to_tpl.model")

makeEntitiesMigration "215"
    $(modelFile "migrations/2020_02_09_rd_point_to_ro.model")

makeEntitiesMigration "223"
    $(modelFile "migrations/2020_02_09_tup_mig.model")

makeEntitiesMigration "227"
    $(modelFile "migrations/2020_02_10_rm_point_to_ro.model")

makeEntitiesMigration "238" $(modelFile "migrations/2020_04_10_rt_rd.model")

makeEntitiesMigration "241"
    $(modelFile "migrations/2020_05_12_fwd_sender_mig.model")

makeEntitiesMigration "247"
    $(modelFile "migrations/2020_05_16_tcl_mig.model")

makeEntitiesMigration "255" $(modelFile "migrations/2020_05_28_tda_mig.model")

makeEntitiesMigration "260" $(modelFile "migrations/2020_06_01_tdc_mig.model")

makeEntitiesMigration "263" $(modelFile "migrations/2020_06_02_tdp.model")

makeEntitiesMigration "266"
    $(modelFile "migrations/2020_06_15_td_accept.model")

makeEntitiesMigration "276"
    $(modelFile "migrations/2020_07_27_ticket_resolve_mig.model")

makeEntitiesMigration "280"
    $(modelFile "migrations/2020_08_10_bundle_mig.model")

makeEntitiesMigration "282"
    $(modelFile "migrations/2020_08_13_vcs.model")

makeEntitiesMigration "285"
    $(modelFile "migrations/2022_06_14_collab_mig.model")

makeEntitiesMigration "289"
    $(modelFile "migrations/2022_07_17_project_actor.model")

makeEntitiesMigration "297"
    $(modelFile "migrations/2022_07_24_project_create.model")

makeEntitiesMigration "300"
    $(modelFile "migrations/2022_07_25_collab_fulfills_mig.model")

makeEntitiesMigration "303"
    $(modelFile "migrations/303_2022-08-04_username.model")

makeEntitiesMigration "308"
    $(modelFile "migrations/308_2022-08-04_remove_tcr.model")

makeEntitiesMigration "310"
    $(modelFile "migrations/310_2022-08-04_move_ticket_discuss.model")

makeEntitiesMigration "312"
    $(modelFile "migrations/312_2022-08-04_move_ticket_followers.model")

makeEntitiesMigration "316"
    $(modelFile "migrations/316_2022-08-04_move_ticket_accept.model")

makeEntitiesMigration "318"
    $(modelFile "migrations/318_2022-08-04_tal_ticket.model")

makeEntitiesMigration "323"
    $(modelFile "migrations/323_2022-08-04_tar_ticket.model")

makeEntitiesMigration "328"
    $(modelFile "migrations/328_2022-08-04_tjl_ticket.model")

makeEntitiesMigration "332"
    $(modelFile "migrations/332_2022-08-04_trl_ticket.model")

makeEntitiesMigration "338"
    $(modelFile "migrations/338_2022-08-04_rtd_child.model")

makeEntitiesMigration "342"
    $(modelFile "migrations/342_2022-08-04_ltd_parent.model")

makeEntitiesMigration "345"
    $(modelFile "migrations/345_2022-08-04_tdcl_child.model")

makeEntitiesMigration "348"
    $(modelFile "migrations/348_2022-08-04_tr_ticket.model")

makeEntitiesMigration "356"
    $(modelFile "migrations/356_2022-08-04_person_actor.model")

makeEntitiesMigration "365"
    $(modelFile "migrations/365_2022-08-04_group_actor.model")

makeEntitiesMigration "367"
    $(modelFile "migrations/367_2022-08-04_repo_actor.model")

makeEntitiesMigration "388"
    $(modelFile "migrations/388_2022-08-04_ticket_loom.model")

makeEntitiesMigration "396"
    $(modelFile "migrations/396_2022-08-04_repo_dir.model")

makeEntitiesMigration "409"
    $(modelFile "migrations/409_2022-08-05_repo_create.model")

makeEntitiesMigration "414"
    $(modelFile "migrations/414_2022-08-05_followremote_actor.model")

makeEntitiesMigration "418"
    $(modelFile "migrations/418_2022-08-06_follow_actor.model")

makeEntitiesMigration "426"
    $(modelFile "migrations/426_2022-08-21_collab_accept_mig.model")

makeEntitiesMigration "429"
    $(modelFile "migrations/429_2022-08-30_collab_repo.model")

makeEntitiesMigration "430"
    $(modelFile "migrations/430_2022-08-30_collab_deck.model")

makeEntitiesMigration "431"
    $(modelFile "migrations/431_2022-08-30_collab_loom.model")

makeEntitiesMigration "447"
    $(modelFile "migrations/447_2022-08-30_collab_accept.model")

makeEntitiesMigration "466"
    $(modelFile "migrations/466_2022-09-04_collab_topic_repo.model")

makeEntitiesMigration "467"
    $(modelFile "migrations/467_2022-09-04_collab_topic_deck.model")

makeEntitiesMigration "468"
    $(modelFile "migrations/468_2022-09-04_collab_topic_loom.model")

makeEntitiesMigration "486"
    $(modelFile "migrations/486_2022-09-04_collab_enable.model")

makeEntitiesMigration "495"
    $(modelFile "migrations/495_2022-09-21_ticket_title.model")

makeEntitiesMigration "498"
    $(modelFile "migrations/498_2022-10-03_forwarder.model")

makeEntitiesMigration "504"
    $(modelFile "migrations/504_2022-10-16_message_author.model")

makeEntitiesMigration "507"
    $(modelFile "migrations/507_2022-10-16_workflow.model")

makeEntitiesMigration "515"
    $(modelFile "migrations/515_2022-10-19_inviter_local.model")

makeEntitiesMigration "520"
    $(modelFile "migrations/520_2022-10-19_inviter_remote.model")

makeEntitiesMigration "525"
    $(modelFile "migrations/525_2022-10-19_collab_accept_local.model")

makeEntitiesMigration "527"
    $(modelFile "migrations/527_2022-10-20_collab_accept_remote.model")
