{- This file is part of Vervis.
 -
 - Written in 2018, 2019, 2020, 2022 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Migration.Entities
    ( model_2016_08_04
    , model_2016_09_01_just_workflow
    , model_2016_09_01_rest
    , model_2019_02_03_verifkey
    , model_2019_03_19
    , model_2019_03_30
    , model_2019_04_11
    , model_2019_04_12
    , model_2019_04_22
    , model_2019_05_03
    , model_2019_05_17
    , model_2019_06_06
    , model_2019_09_25
    , model_2019_11_04
    , model_2020_01_05
    , model_2020_02_05
    , model_2020_02_07
    , model_2020_02_09
    , model_2020_02_22
    , model_2020_04_07
    , model_2020_04_09
    , model_2020_05_12
    , model_2020_05_16
    , model_2020_05_17
    , model_2020_05_25
    , model_2020_05_28
    , model_2020_06_01
    , model_2020_06_18
    , model_2020_07_23
    , model_2020_07_27
    , model_2020_08_10
    , model_2022_06_14
    , model_2022_07_17
    , model_2022_07_24
    , model_384_loom
    , model_386_assignee
    , model_399_fwder
    , model_408_collab_loom
    , model_425_collab_accept
    , model_428_collab_topic_local
    , model_451_collab_remote_accept
    , model_453_collab_receive
    , model_494_mr_origin
    , model_497_sigkey
    , model_508_invite
    , model_530_join
    )
where

import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Time (UTCTime)
import Database.Persist.Class (EntityField, Unique)
import Database.Persist.EmailAddress ()
import Database.Persist.Schema.Types (Entity)
import Database.Persist.Schema.SQL ()
import Database.Persist.Schema.TH (makeEntitiesMigration)
import Database.Persist.Sql (SqlBackend)
import Text.Email.Validate (EmailAddress)
import Web.Text (HTML, PandocMarkdown)

import Crypto.ActorKey
import Development.PatchMediaType
import Development.PatchMediaType.Persist

import Vervis.FedURI
import Vervis.Migration.TH (schema)
import Vervis.Model.Group
import Vervis.Model.Ident
import Vervis.Model.Role
import Vervis.Model.TH
import Vervis.Model.Ticket
import Vervis.Model.Workflow

-- For migrations 77, 114

import Data.Int

import Database.Persist.JSON
import Network.FedURI
import Web.ActivityPub

type PersistActivity = PersistJSON (Doc Activity URIMode)

model_2016_08_04 :: [Entity SqlBackend]
model_2016_08_04 = $(schema "2016_08_04")

model_2016_09_01_just_workflow :: [Entity SqlBackend]
model_2016_09_01_just_workflow = $(schema "2016_09_01_just_workflow")

model_2016_09_01_rest :: [Entity SqlBackend]
model_2016_09_01_rest = $(schema "2016_09_01_rest")

model_2019_02_03_verifkey :: [Entity SqlBackend]
model_2019_02_03_verifkey = $(schema "2019_02_03_verifkey")

model_2019_03_19 :: [Entity SqlBackend]
model_2019_03_19 = $(schema "2019_03_19")

model_2019_03_30 :: [Entity SqlBackend]
model_2019_03_30 = $(schema "2019_03_30")

model_2019_04_11 :: [Entity SqlBackend]
model_2019_04_11 = $(schema "2019_04_11")

model_2019_04_12 :: [Entity SqlBackend]
model_2019_04_12 = $(schema "2019_04_12")

model_2019_04_22 :: [Entity SqlBackend]
model_2019_04_22 = $(schema "2019_04_22")

model_2019_05_03 :: [Entity SqlBackend]
model_2019_05_03 = $(schema "2019_05_03")

model_2019_05_17 :: [Entity SqlBackend]
model_2019_05_17 = $(schema "2019_05_17")

model_2019_06_06 :: [Entity SqlBackend]
model_2019_06_06 = $(schema "2019_06_06")

model_2019_09_25 :: [Entity SqlBackend]
model_2019_09_25 = $(schema "2019_09_25")

model_2019_11_04 :: [Entity SqlBackend]
model_2019_11_04 = $(schema "2019_11_04")

model_2020_01_05 :: [Entity SqlBackend]
model_2020_01_05 = $(schema "2020_01_05")

model_2020_02_05 :: [Entity SqlBackend]
model_2020_02_05 = $(schema "2020_02_05_local_ticket")

model_2020_02_07 :: [Entity SqlBackend]
model_2020_02_07 = $(schema "2020_02_07_tpl")

model_2020_02_09 :: [Entity SqlBackend]
model_2020_02_09 = $(schema "2020_02_09_tup")

model_2020_02_22 :: [Entity SqlBackend]
model_2020_02_22 = $(schema "2020_02_22_tpr")

model_2020_04_07 :: [Entity SqlBackend]
model_2020_04_07 = $(schema "2020_04_07_tpra")

model_2020_04_09 :: [Entity SqlBackend]
model_2020_04_09 = $(schema "2020_04_09_rt")

model_2020_05_12 :: [Entity SqlBackend]
model_2020_05_12 = $(schema "2020_05_12_fwd_sender")

model_2020_05_16 :: [Entity SqlBackend]
model_2020_05_16 = $(schema "2020_05_16_tcl")

model_2020_05_17 :: [Entity SqlBackend]
model_2020_05_17 = $(schema "2020_05_17_patch")

model_2020_05_25 :: [Entity SqlBackend]
model_2020_05_25 = $(schema "2020_05_25_fwd_sender_repo")

model_2020_05_28 :: [Entity SqlBackend]
model_2020_05_28 = $(schema "2020_05_28_tda")

model_2020_06_01 :: [Entity SqlBackend]
model_2020_06_01 = $(schema "2020_06_01_tdc")

model_2020_06_18 :: [Entity SqlBackend]
model_2020_06_18 = $(schema "2020_06_18_tdo")

model_2020_07_23 :: [Entity SqlBackend]
model_2020_07_23 = $(schema "2020_07_23_remote_collection_reboot")

model_2020_07_27 :: [Entity SqlBackend]
model_2020_07_27 = $(schema "2020_07_27_ticket_resolve")

model_2020_08_10 :: [Entity SqlBackend]
model_2020_08_10 = $(schema "2020_08_10_bundle")

model_2022_06_14 :: [Entity SqlBackend]
model_2022_06_14 = $(schema "2022_06_14_collab")

model_2022_07_17 :: [Entity SqlBackend]
model_2022_07_17 = $(schema "2022_07_17_actor")

model_2022_07_24 :: [Entity SqlBackend]
model_2022_07_24 = $(schema "2022_07_24_collab_fulfills")

model_384_loom :: [Entity SqlBackend]
model_384_loom = $(schema "384_2022-08-04_loom")

model_386_assignee :: [Entity SqlBackend]
model_386_assignee = $(schema "386_2022-08-04_assignee")

model_399_fwder :: [Entity SqlBackend]
model_399_fwder = $(schema "399_2022-08-04_fwder")

model_408_collab_loom :: [Entity SqlBackend]
model_408_collab_loom = $(schema "408_2022-08-04_collab_loom")

model_425_collab_accept :: [Entity SqlBackend]
model_425_collab_accept = $(schema "425_2022-08-21_collab_accept")

model_428_collab_topic_local :: [Entity SqlBackend]
model_428_collab_topic_local = $(schema "428_2022-08-29_collab_topic_local")

model_451_collab_remote_accept :: [Entity SqlBackend]
model_451_collab_remote_accept = $(schema "451_2022-08-30_collab_remote_accept")

model_453_collab_receive :: [Entity SqlBackend]
model_453_collab_receive = $(schema "453_2022-09-01_collab_receive")

model_494_mr_origin :: [Entity SqlBackend]
model_494_mr_origin = $(schema "494_2022-09-17_mr_origin")

model_497_sigkey :: [Entity SqlBackend]
model_497_sigkey = $(schema "497_2022-09-29_sigkey")

model_508_invite :: [Entity SqlBackend]
model_508_invite = $(schema "508_2022-10-19_invite")

model_530_join :: [Entity SqlBackend]
model_530_join = $(schema "530_2022-11-01_join")
